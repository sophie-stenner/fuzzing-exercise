#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char *stringReplace(char *str) {
  int counter = 0; // used to count occurrences
  for (int i = 0; i < strlen(str); i++) {
    if (str[i] == '&') {
      counter += 3;
    } else if (str[i] == '<' || str[i] == '>') {
      counter += 2;
    }
  }

  // declare new string on heap
  char *newString = (char *)malloc(sizeof(char) * (strlen(str) + counter + 1));

  // reset counter to find correct positions in string
  counter = 0;

  for (int i = 0; i < strlen(str); i++) {
    if (str[i] == '&') {
      newString[i + counter] = '&';
      newString[i + counter + 1] = 'a';
      newString[i + counter + 2] = 'm';
      newString[i + counter + 3] = 'p';
      counter += 3;

    } else if (str[i] == '<') {
      newString[i + counter] = '&';
      newString[i + counter + 1] = 'l';
      newString[i + counter + 2] = 't';
      counter += 2;

    } else if (str[i] == '>') {
      newString[i + counter] = '&';
      newString[i + counter + 1] = 'g';
      newString[i + counter + 2] = 't';
      counter += 2;
    }
    // just copy value
    else {
      newString[i + counter] = str[i];
    }
  }

  newString[strlen(str) + counter] = '\0';

  return newString;
}
