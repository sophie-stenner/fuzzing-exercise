#include "utility.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
  printf("These are the old strings:\n\n");
  char *str1 = "hello & goodbye";
  char *str2 = "1 + 1 < 5 && 1 + 1 > 1";
  char *str3 = "dd&&&&&&&&";

  printf("%s\n", str1);
  printf("%s\n", str2);
  printf("%s\n", str3);

  printf("These are the new strings:\n\n");

  char *result1 = stringReplace(str1);
  printf("%s\n", &result1[0]);
  free(result1);
  char *result2 = stringReplace(str2);
  printf("%s\n", &result2[0]);
  free(result2);
  char *result3 = stringReplace(str3);
  printf("%s\n", &result3[0]);
  free(result3);
}
