#include "utility.h"
#include <assert.h>
#include <string.h>

int main() {
  char *string1 = stringReplace("hello & goodbye");
  assert(strcmp(string1, "hello &amp goodbye") == 0);
  free(string1);

  char *string2 = stringReplace("");
  assert(strcmp(string2, "") == 0);
  free(string2);
  char *string3 = stringReplace("1 + 1 < 5 && 1 + 1 > 1");
  assert(strcmp(string3, "1 + 1 &lt 5 &amp&amp 1 + 1 &gt 1") == 0);
  free(string3);

  char *string4 = stringReplace("The end.");
  assert(strcmp(string4, "The end.") == 0);
  free(string4);
}
